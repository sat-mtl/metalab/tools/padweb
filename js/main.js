// Filename: main.js

// Require.js allows us to configure shortcut alias
// There usage will become more apparent further along in the tutorial.
require.config({
  paths: {
    underscore: '/assets/libs/underscore-min', 
    backbone: 'libs/backbone-min',
    // jqueryui: '/assets/libs/jqueryui/js/jquery-ui-1.10.2.custom.min',
    debounce : '/assets/libs/jquery.debounce',
    mobile : '/assets/libs/jquery.mobile-1.3.1.min',
    hammerjs: '/assets/libs/hammer.min',
    // jhammer: '/assets/libs/jquery.hammer',
    socketio: '/socket.io/socket.io',
    util: 'lib/util',
    events: 'lib/events',
    handlers: 'lib/handlers',
    lodash: 'lib/lodash.min',
    config: 'lib/config',
    // punch : '/assets/libs/jquery.ui.touch-punch.min'
  },
  shim: {
    underscore:{
      exports : '_'
    },
    backbone: {
      deps : ["underscore", "jquery"],
      exports : "Backbone"
    },
    mobile: {
      deps : ["jquery"],
      exports : "mobile"
    },
    hammerjs: {
      exports: 'Hammer'
    },
    // jhammer: {
    //   deps: ["hammerjs", "jquery"],
    //   expports: "jhammer"
    // },
    // jqueryui: {
    //   deps : ["jquery", "mobile"],
    //   exports : "jqueryui"
    // }
  },
  waitSeconds: 15
});

require([
  'app',
  socket = io.connect()
], function( App ){
  App.initialize();
});

