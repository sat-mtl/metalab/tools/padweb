// Filename: app.js
define([
  'underscore',
  'jquery',
  'debounce',
  'mobile',
  'lib/socket',
  'util',
  'events',
  'config'
], function(_, $, debounce, mobile, socket, util, events, config) {
  
  var widthW = $(document).width()
  ,   heightW = $(document).height()
  ,   nbPage = $(".page").length
  ,   currentPage = 0

  function initialize() {
    "use strict";
    // var agent = navigator.userAgent.toLowerCase();
    //  if (isAndroid) {
    //   $("#agent").html("Android");
    //   handleAndroid();
    // } else if (isIPhone) {
    //   $("#agent").html("iPhone");
    //   handleAndroid();
    // } else {
    //   $("#agent").html("unsupported");
    // }

    /* *************************************
     * configure some jquery internals
     */
    // $.event.special.swipe.horizontalDistanceThreshold = config.swipeDistanceThreshold;
    // $.event.special.swipe.durationThreshold = config.swipeDurationThreshold;

    console.log(util.getPlatform(config.supportedPlatforms));
    // console.log(util.getPlatform(supportedPlatforms));

    console.log("page width: " + widthW);
    document.ontouchmove = function(event) {
      event.preventDefault();
    }
    

    // Get orientation delta from server
    // var orientationDelta = 0.0;

    // socket.on("orientationDelta", function(data) {
    //   orientationDelta = parseFloat(data);
    // });

    // socket.emit("getOrientationDelta", 0);

    //$("#global").width(widthW).height(heightW);
    //$("#content").width(widthW*nbPage).height(heightW);
    $(".page").height(heightW).width(widthW);
    $("#arrows").height(heightW).width(widthW);
    $("#compass").height(heightW/6).width(widthW/6);
    // $("#calibrationBtn").on("touchstart", events.getCalibrationButton);
    $("#calibrationBtn").on("click", events.getCalibrationButton);
    // $("#calibrateRotation-slider").on("change", function() {
    //   console.log("calibration ", $(this).val());
    // });

    events.init(widthW, heightW);
  }; // end initialize

  return {
    initialize: initialize
  };

});
