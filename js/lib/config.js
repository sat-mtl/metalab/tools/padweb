define({
  updateRate: 33,
  speed: 0.0,
  calibrationOn: 0,
  slideOn: true,
  supportedPlatforms: ['android', 'iphone', 'linux'],
  swipeDistanceThreshold: 200,
  swipeDurationThreshold: 150,
  swipeVeloctityThreshold: 0.65
});
