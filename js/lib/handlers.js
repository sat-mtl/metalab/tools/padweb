


define([
  'jquery',
  'lib/socket',
], function ($, socket, gyro) {
  // initialize the motion/orientation object
  var currentPage = 0;
  var speed = 0.0;
  var orientationDelta = 0.0;

  socket.on("orientationDelta", function(data) {
    // console.log("socket on orientationDelta: ", data);
    orientationDelta = parseFloat(data);
  });
  
  socket.emit("getOrientationDelta", 0);

  var handleMove = function(evt) {
    evt.preventDefault();
    // changedTouches is burried because we use hammer
    var touches = evt.srcEvent.touches;
    var py = touches[0].pageY;
    var h = $(document).height();
    var touchPos = Math.max(h - py, 0);
    var distFromMid = touchPos - (h / 2);
    var speed;
    if (distFromMid > 0.5) {
      speed = 0.01;
    } else if (distFromMid < -0.5) {
      speed = -0.01;
    } else {
      speed = 0.0;
    }
    if (Math.abs(distFromMid) > 10) {
      speed = (speed * Math.pow(distFromMid, 2)) * 0.5;
    } else {
      speed += distFromMid * 0.1;
    }
    if ($.mobile.activePage[0].id == "navigation") {
      // console.log("speed: ", speed);
      socket.emit("speed", speed);
    }
  };

  
  var handleStop = function() {
    speed = 0.0;
    socket.emit('speed', speed);
  };
  
  var orientationToYPR = function (event) {
    var orientation = event;
    //var orientation = deviceOrientation(event);
    var heading;
    var yaw;
    if(orientation.webkitCompassHeading) {
      heading = orientation.webkitCompassHeading; 
      yaw = (( heading + orientationDelta) % 360.0) * 0.0174532925199;
      // $("#footer").html("yaw via webkit: " + Math.abs(yaw) + "</br> original value: " + heading);

    }
    else {
      heading = orientation.alpha;
      yaw = ((-(heading + orientationDelta) -210) % 360.0) * 0.0174532925199;
      // $("#footer").html("yaw via alpha: " + yaw+ "</br> original value: " + heading);
      // var yaw = orientation.alpha * 0.0174532925199 + (Math.PI/2);
    }
    var pitch = orientation.beta * 0.0174532925199;
    var roll = orientation.gamma * 0.0174532925199;
    // console.log("yaw: ", yaw);
    return {
      yaw: yaw,
      pitch: pitch,
      roll: roll
    }
  };
  
  var yprToVector = function(ypr) {
    var x, y, z;
    x = Math.sin(-ypr.pitch + 1.570796326795) * Math.cos(-ypr.yaw);
    y = Math.sin(-ypr.pitch + 1.570796326795) * Math.sin(-ypr.yaw);
    z = Math.cos(-ypr.pitch + 1.570796326795);
    return {
      x: x,
      y: y,
      z: z
    }
  };

  

  var handleOrientation = function(orientation) {
    var ypr = orientationToYPR(orientation);
    var ret = yprToVector(ypr);
    var rot = ypr.yaw * 180 / Math.PI;
    if ($.mobile.activePage[0].id == "navigation") {
      socket.emit("spinVelocity", ret);
      socket.emit("deviceOrientation", ypr);
      // console.log("rotation: ", rot);
      // document.getElementById("compass").style.transform = "rotate("+ rot +"deg)";
      // document.getElementById("compass").style.webkitTransform = "rotate("+ rot +"deg)";
      // document.getElementById("info").innerHTML = Math.round(rot);
    }
    // return ret;
  };

  var handleDoubleTap = function(event) {
    socket.emit("doubleTap", event);
    console.log("doubletap", event);
  }

  var handleCalibrationActions = function(state, id, value) {
    var show = state ? 1 : 0;
    if ($.mobile.activePage[0].id == "calibration") {
      console.log("handlers: ", id, value);
      if (id == "calibrateRotation-slider") {
        socket.emit("calibrate", show, "orientation", value)
      };
      if (id == "calibrateHeight-slider") {
        socket.emit("calibrate", show, "height", value)
      };
      socket.emit("showOwnMesh", show);
      // console.log("calibrate", show);
    }
  };

  var handleSwipeLeft = function (e) {
    var nextpage = $(".page").next('div[data-role="page"]');
    // console.log("next page: ", nextpage);
    if (nextpage.length > 0) {
      $.mobile.changePage(nextpage, {transition: "flow"});
    }
  }
  
  var handleSwipeRight = function (e) {
    var prevpage = $(".page").prev('div[data-role="page"]');
    console.log("previous page: ", prevpage);
    if (prevpage.length > 0) {
      $.mobile.changePage(prevpage, {transition: "flow", reverse: true});
    }
  }


  return {
    handleMove: handleMove,
    handleStop: handleStop,
    handleOrientation: handleOrientation,
    handleCalibrationActions: handleCalibrationActions,
    handleSwipeLeft: handleSwipeLeft,
    handleSwipeRight: handleSwipeRight,
    handleDoubleTap: handleDoubleTap
  }

});
