define(function() {
  /* 
   *  Figure out what platform we are on 
   */

  var getPlatform = function(platforms) {
    var agent = navigator.userAgent.toLowerCase();
    console.log(agent);
    var ret = "unrecognized platform";
    for (var i=0;i<platforms.length;i++){
      var platform = agent.indexOf(platforms[i]);
      if (platform > -1) {
        ret = platforms[i];
      } 
    }
    return ret;
  }
  
  return {
    getPlatform: getPlatform
  };
});

