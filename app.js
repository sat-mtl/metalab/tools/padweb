var config = {
  address : "localhost",
  serverPort: 8088,
  oscAddress : "/posture/cmd",
  oscPort : 54324,
  brushAddress : "/posture/brush",
  surfaceAddress : "/posture/surface",
  calibrationAddress : "/scene/camera",
  oscURI : "/scene",
  oscDefault : '/padweb/',
  orientationDelta : 0,
  enableOrientation : true
}
process.argv.forEach(function (val, index, array) {
  if (val == "-h") {
    console.log("\npadweb  - OSC 3D space navigator\n");
    console.log(" -h              Print this help");
    console.log(" -H              Destination's host or IP address");
    console.log(" -p              OSC port number");
    console.log(" -P              server port number");
    console.log(" -o              Drawingspace options");
    console.log(" -s              Full spin OSC URL (i.e. /SPIN/sceneid/userid)");
    console.log(" -alpha          Virtual world rotation coefficiont accordng to true north");
    console.log(" -orientation    User orientation");
    process.exit();
  }    
  if (val == "-ip")
    config.address = process.argv[index+1];
  if (val == "-p")
    config.oscPort = process.argv[index+1];
  if (val == "-P")
    config.serverPort = process.argv[index+1];
  if (val == "-o") {
    config.oscAddress = config.oscAddress + "_" + process.argv[index+1];
    config.brushAddress = config.brushAddress + "_" + process.argv[index+1];
    config.surfaceAddress = config.surfaceAddress + "_" + process.argv[index+1];
    config.calibrationAddress = config.calibrationAddress + "_" + process.argv[index+1];

  }
  if (val == "-s")
    config.oscURI = process.argv[index+1];
  if (val == "-alpha")
    config.orientationDelta = process.argv[index+1];
  if (val == "-orientation")
    config.enableOrientation = true;
});

var express = require("express")
, app = express()
, http = require('http')
, server = http.createServer(app).listen(config.serverPort)
, io = require('socket.io').listen(server, { log: false })
, sys = require('sys')
, readline = require('readline')
, freeport = require("freeport")
, osc = require('node-osc');

io.set('browser client gzip', true);
io.set('browser client minification', true);  // send minified client
io.set('browser client etag', true);          // apply etag caching logic based on version number

//*** PARAM EXPRESS ***//
app.use("/assets", express.static(__dirname + "/assets"));
app.use("/js", express.static(__dirname + "/js"));
app.get('/', function (req, res){ res.sendfile(__dirname + '/index.html'); });
app.get('/sato', function (req, res){ res.sendfile(__dirname + '/sato.html'); });
app.get('/test', function (req, res){ res.sendfile(__dirname + '/test.html'); });
app.get('/droid', function (req, res){ res.sendfile(__dirname + '/droid.html'); });


function puts(error, stdout, stderr) {
  sys.puts(stdout)
}

var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});


console.log("sending OSC to " + " " + config.oscAddress + " | osc path: " + config.oscURI + " " + config.address + " | osc port: " + config.oscPort)

///*** OSC clients ***///
var client = new osc.Client("localhost", config.oscPort);
//console.log("Address OSC : " + config.address + config.oscAddress, "port : " + config.oscPort);
client.send(config.oscAddress, "test send message"); //test send

// var client = new osc.Client(config.address, 54324);
// var blenderClient = new osc.Client(config.address, 54324);

///*** SOCKET.IO ***///
io.sockets.on('connection', function (socket) {
  var speed = 0.0;

  socket.on("getOrientationDelta", function(state) {
    socket.emit("orientationDelta", config.orientationDelta);
  });

  socket.on("pressBtn", function(state) {
    client.send(config.oscAddress, state);
  });

  socket.on("pressBrush", function(state) {
    client.send(config.brushAddress, state);
  });

  socket.on("switchProjection", function(state) {
    client.send(config.surfaceAddress, state);
  });
  socket.on("calibrate", function(stateMirror, target, value) {
    client.send(config.calibrationAddress, "calibrate", stateMirror, target, parseFloat(value));
  });
  socket.on("showOwnMesh", function(stateMirror) {
    // console.log(config.calibrationAddress)
    client.send(config.calibrationAddress, "calibrate", stateMirror);
  });
  socket.on("speed", function(state) {
    speed = state;
    // if (speed*speed>2){
    //   speed += speed * 1.5;
    // }
  });


  socket.on("orientation", function(state) {

    // console.log("orientation", state);
    //client.send(oscDefault+'orientation', state.yaw, state.pitch, state.roll);

  });


  socket.on("spinOrientation", function(state) {
    //if (enableOrientation)
    //client.send(oscURI, "setOrientation", state.pitch, 0.00000001, state.yaw);
  });

  socket.on("spinVelocity", function(state) {
    // console.log("---> ", config.oscURI, "setVelocity", state.x * speed, state.y * speed, state.z * speed)
    client.send(config.oscURI, "setVelocity", state.x * speed, state.y * speed, state.z * speed);
    //blenderClient.send(config.oscBlender, "setVelocity", state.x * speed, state.y * speed, state.z * speed);
  });

  socket.on("deviceOrientation", function(orientation) {
    client.send(config.oscURI, "setOrientation", orientation.yaw, orientation.pitch, orientation.roll);
  });

  socket.on("doubleTap", function(data){
    // console.log("doubletap: ", data);
    client.send(config.oscURI, "doubleTap");
  });

  /** OSC FOR SATO COMMANDS **/

  socket.on("direction", function(x, y) {
    client.send(config.oscDefault+'direction', x, y);
  });

  socket.on('button', function(id, status) {
    client.send(config.oscDefault+'buttons', id, status);
  });

  socket.on('debug', function(data) {
    client.send(config.oscURI, "DEBUG", data);
  });

});
